
package com.springcookbook.aspect;

import java.util.Date;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 *
 * @author joaocassamano
 */
@Component
@Aspect
public class Aspect1 {
    
    @Around("execution(* com.springcookbook.controller.*.*(..))")
    public Object doBasicProfiling(ProceedingJoinPoint jointPoint) throws Throwable{
        Long t1 = System.currentTimeMillis();
        Object returnValue = jointPoint.proceed();
        Long t2 = System.currentTimeMillis();
        Long executionTime = t2 -t1;
        
        String className = jointPoint.getSignature().getDeclaringTypeName();
        String methodName= jointPoint.getSignature().getName();
        System.out.println(new Date()+"Aspjecj.doBasicProfiling: "+className+"."+methodName+"() took "+executionTime+" ms");
        return returnValue;
    }
    
    @Before("execution(* com.springcookbook.controller.*.*(..))")
    public void logArguments(JoinPoint jointPoint) throws Throwable{
        Object[] arguments = jointPoint.getArgs();
        
        String className = jointPoint.getSignature().getDeclaringTypeName();
        String methodName= jointPoint.getSignature().getName();
        System.out.println(new Date()+"Aspjecj.logArguments: "+"---"+className+"."+methodName+"() ---");
        for(int i =0;i<arguments.length;i++){
            System.out.println(arguments[i]);
        }
    }
    
    @AfterReturning(pointcut="execution(* com.springcookbook.controller.*.*(..))",returning="returnValue")
    public void logReturnValue(JoinPoint jointPoint, Object returnValue) throws Throwable{
        
        String className = jointPoint.getSignature().getDeclaringTypeName();
        String methodName= jointPoint.getSignature().getName();
        System.out.println(new Date()+"Aspjecj.logReturnValue: "+"---"+className+"."+methodName+"() ---");
        System.out.println("returnValue="+returnValue);
        
    }
    
    @AfterThrowing(pointcut="execution(* com.springcookbook.controller.*.*(..))",throwing="exception")
    public void logException(JoinPoint jointPoint, Exception exception) throws Throwable{
        
        String className = jointPoint.getSignature().getDeclaringTypeName();
        String methodName= jointPoint.getSignature().getName();
        System.out.println(new Date()+"Aspjecj.logReturnValue: "+"---"+className+"."+methodName+"() ---");
        System.out.println("exception message: "+exception.getMessage());
        
    }
    
    @After("execution(* com.springcookbook.controller.*.*(..))")
    public void logAfter(JoinPoint jointPoint) throws Throwable{
        String className = jointPoint.getSignature().getDeclaringTypeName();
        String methodName= jointPoint.getSignature().getName();
        System.out.println(new Date()+"Aspjecj.logReturnValue: "+"---"+className+"."+methodName+"() ---");
    }
    
}
