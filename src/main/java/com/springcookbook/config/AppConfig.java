
package com.springcookbook.config;

import com.springcookbook.service.UserServiceRestImpl;
import com.springcookbook.tools.Utils;
import java.util.Locale;
import javax.activation.DataSource;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.mobile.device.site.SitePreferenceHandlerInterceptor;
import org.springframework.mobile.device.switcher.SiteSwitcherHandlerInterceptor;
import org.springframework.mobile.device.view.LiteDeviceDelegatingViewResolver;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

@Configuration
@EnableAspectJAutoProxy
@EnableWebMvc
@ComponentScan
(basePackages={"com.springcookbook.controller",
               "com.springcookbook.service",
               /*"com.springcookbook.aspect"*/
})

/**
 *
 * @author joaocassamano
 */
public class AppConfig extends WebMvcConfigurerAdapter {
    
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Utils utils(){
        return new Utils();
    }
    
    @Bean
    public ViewResolver jspViewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
    
    @Bean
    public HandlerInterceptor performanceInterceptor(){
        PerformanceInterceptor interceptor = new PerformanceInterceptor();
        return interceptor;
    }
    
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(performanceInterceptor())
                .addPathPatterns("/home","/user/list");
        registry.addInterceptor(localeChangeInterceptor());
        registry.addInterceptor(deviceResolverInterceptor());
        registry.addInterceptor(sitePreferenceInterceptor());
        registry.addInterceptor(siteSwitcherHandlerInterceptor());
    }
    
    @Bean
    public MessageSource messageSource(){
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/messages");
        messageSource.setUseCodeAsDefaultMessage(true);
        return messageSource;
    }
    
    @Bean
    public HandlerInterceptor localeChangeInterceptor(){
        LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
        interceptor.setParamName("lang");
        return interceptor;
    }
    
    @Bean
    public LocaleResolver localeResolver(){
        CookieLocaleResolver localeResolver = new CookieLocaleResolver();
        localeResolver.setDefaultLocale(new Locale("en"));
        
        //Template for retrieving current language from controller/interceptor
        /*Locale locale = LocaleContextHolder.getLocale();
        String lang = locale.getLanguage();
        String language = locale.getDisplayLanguage();
        String language2 = locale.getDisplayLanguage(locale);*/
        
        return localeResolver;
    }
    
    @Bean
    MultipartResolver multipartResolver(){
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setMaxUploadSize(500000000);
        return resolver;
    }
    
    @Bean
    public DeviceResolverHandlerInterceptor deviceResolverInterceptor(){
        return new DeviceResolverHandlerInterceptor();
    }
    
    @Bean
    public SitePreferenceHandlerInterceptor sitePreferenceInterceptor(){
        return new SitePreferenceHandlerInterceptor();
    }
    
    @Bean
    public LiteDeviceDelegatingViewResolver liteDeviceDelegatingViewResolver(){
        InternalResourceViewResolver delegate = new InternalResourceViewResolver();
        delegate.setPrefix("/WEB-INF/jsp");
        delegate.setSuffix(".jsp");
        LiteDeviceDelegatingViewResolver resolver = new LiteDeviceDelegatingViewResolver(delegate);
        resolver.setMobilePrefix("mobile/");
        //resolver.setTabletPrefix("tablet");
        resolver.setEnableFallback(true);
        return resolver;
    }
    
    @Bean
   public SiteSwitcherHandlerInterceptor siteSwitcherHandlerInterceptor(){
       //return SiteSwitcherHandlerInterceptor.dotMobi("mywebsite.com");
       //return SiteSwitcherHandlerInterceptor.mDot("mywebsite.com");
       //return SiteSwitcherHandlerInterceptor.standard("mywebsite.com","mymobilewebsite.com",".mywebsite.com");
       return SiteSwitcherHandlerInterceptor.urlPath("/mobile","springwebapp");
   }
   
   @Bean
   public UserServiceRestImpl userServiceRestImpl(){
       return new UserServiceRestImpl();
   }
    
    /*
    @Bean
    public TilesConfigurer tilesConfigurer(){
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        final String[] definitions  = { "/WEB-INF/tiles.xml" };
        tilesConfigurer.setDefinitions(definitions);
        return tilesConfigurer;
    }
    
    @Bean
    public ViewResolver tilesViewResolver(){
        TilesViewResolver resolver = new TilesViewResolver();
        return resolver;
    }*/
    
}
