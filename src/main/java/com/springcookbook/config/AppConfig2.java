
package com.springcookbook.config;

//import com.springcookbook.tools.Utils;
import com.springcookbook.dao.UserDAO;
import com.springcookbook.model.User;
import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;



/**
 *
 * @author joaocassamano
 */
@Configuration
@EnableTransactionManagement
public class AppConfig2 {
    
    @Bean
    public javax.sql.DataSource dataSource(){
        
        Properties props = new Properties();
        /*props.setProperty("user", "app");
        props.setProperty("password", "appdbpass");
        props.setProperty("useSSL","true");*/
        DriverManagerDataSource dataSource = new DriverManagerDataSource();//TODO: DriverManagerDataSource is old, consult spring docs for updated code
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://192.168.99.100:3307/gdb?autoReconnect=true&amp;useUnicode=true&amp;useSSL=true&amp;useJDBCCompliantTimezoneShift=true&amp;useLegacyDatetimeCode=false&amp;serverTimezone=UTC");
        //dataSource.setUrl("jdbc:mysql://192.168.99.100:3307/gdb");
        dataSource.setUsername("app");
        dataSource.setPassword("appdbpass");
        //dataSource.setUsername("root");
        //dataSource.setPassword("rootdbpass");
        //dataSource.setConnectionProperties(props);
        return dataSource;
    }
    
    @Bean
    public JdbcTemplate jdbcTemplate(javax.sql.DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
    
    @Bean
   public UserDAO userDAO(){
       return new UserDAO();
   }
   
   @Bean
   public DataSourceTransactionManager transactionManager(){
       DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
       transactionManager.setDataSource(dataSource());
       return transactionManager;
   }
   
   @Bean
   public SessionFactory sessionFactory(javax.sql.DataSource dataSource){
       LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
       
       Properties props = new Properties();
       props.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
       props.put("hibernate.show_sql", "true");
       sessionBuilder.addProperties(props);
       
       sessionBuilder.addAnnotatedClass(User.class);
       
       return sessionBuilder.buildSessionFactory();
   }
   
   @Bean
   public HibernateTransactionManager transactionManager(SessionFactory sessionFactory){
       return new HibernateTransactionManager(sessionFactory);
   }
    
}
