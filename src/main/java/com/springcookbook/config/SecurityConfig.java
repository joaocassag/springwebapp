
package com.springcookbook.config;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
//import org.springframework.security.

/**
 *
 * @author joaocassamano
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    public void configureUsers(AuthenticationManagerBuilder auth) throws Exception{
        System.out.println(new Date()+" entered configureUsers");
        auth.inMemoryAuthentication()
                .withUser("user1").password("pwd").roles("USER")
                .and()
                .withUser("admin").password("admin_pwd").roles("USER","ADMIN");
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception{
        System.out.println(new Date()+" entered configure ");
        http.authorizeRequests()
                .antMatchers("/css/**","/js/**","/img/**").permitAll()
                //.antMatchers("/users/**").permitAll()
                .antMatchers("/user/addUser/**").permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated();
        http.formLogin().loginPage("/user/login").permitAll();
        
        AntPathRequestMatcher pathRequestMatcher = new AntPathRequestMatcher("/logout");
        http.logout().logoutRequestMatcher(pathRequestMatcher);
        
        http.csrf().disable();
        /*http.csrf()
                .csrfTokenRepository(csrfTokenRepository());*/
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception{
        web
            .ignoring()
                .antMatchers("/css/**")
                .antMatchers("/js/**")
                .antMatchers("/img/**");
    }
    /*
    private CsrfTokenRepository csrfTokenRepository() { 
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository(); 
        repository.setSessionAttributeName("_csrf");
        return repository; 
    }
    */
}
