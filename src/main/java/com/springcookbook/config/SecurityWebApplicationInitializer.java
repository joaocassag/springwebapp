
package com.springcookbook.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * @author joaocassamano
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
    
}
