
package com.springcookbook.controller;

import com.springcookbook.service.UserService;
import com.springcookbook.service.UserServiceImpl1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author joaocassamano
 */
@Controller
public class HelloController {
    
    @Autowired
    UserServiceImpl1 userService;
    
    @Autowired
    private ApplicationContext appContext;
    
    @RequestMapping("hi")
    @ResponseBody
    public String hi(){
        //return "Hello, World.";
        UserService aService = (UserService) appContext.getBean("aUserService1");
        String aService_string = "number of users using appContext: " + aService.findUsers();
        String aService1_string = "number of users using userService: " + aService.findUsers();
        return aService_string+"\n"
               +aService1_string;
    }
}
