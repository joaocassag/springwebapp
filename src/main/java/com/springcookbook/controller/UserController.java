
package com.springcookbook.controller;

import com.springcookbook.dao.UserDAO;
import com.springcookbook.model.User;
//import com.springcookbook.service.UserService;
import com.springcookbook.service.UserServiceImpl1;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.mobile.device.site.SitePreferenceUtils;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author joaocassamano
 */
@Controller
//@RestController
@RequestMapping("/user")
public class UserController {
    
    @Autowired
    private UserDAO userDAO;
    
    @Autowired
    UserServiceImpl1 userService;
    
    @Autowired
    private ApplicationContext appContext;
    
    @RequestMapping("/login")
    public String login(){
        System.out.println(new Date()+" entered login page");
        return "/user/login";
    }
    
    /*
    @RequestMapping(value="/login",method=RequestMethod.POST)
    public String login(@RequestParam(value="username") String username, 
            @RequestParam(value="password") String password){
        System.out.println(new Date()+" entered login(POST) ");
        System.out.println("username is "+username);
        //System.out.println("uploaded file");
        return "/user/days";
    }*/
    
    @RequestMapping("/list")
    public void userList(Model model){
        System.out.println(new Date()+" entered userList");
        model.addAttribute("nOfUsers",6);
    }
    
    @RequestMapping("{id}/{field}")
    public String showUserField(Model model, @PathVariable("id") Long userId, @PathVariable("field") String field){
        System.out.println(new Date()+" entered /user/{id}/{field}");
        model.addAttribute("id",7);
        model.addAttribute("field",field);
        
        return "/user/list";
    }
    
    @RequestMapping("/days")
    public void days(){
        System.out.println(new Date()+" entered day page");
        //return "days";
    }
    
    //@RequestMapping("{en|fr/pt}/")
    @RequestMapping//("/")
    public String home(){
        System.out.println(new Date()+" entered home");
        return "home";
    }
    
    @RequestMapping("/addUser")
    public String addUser(HttpServletRequest request){
        
        Device currentDevice = DeviceUtils.getCurrentDevice(request);
        if(currentDevice == null){
            
        }
        
        if(currentDevice.isMobile()){
        
        }
        
        if(currentDevice.isTablet()){
        
        }
        
        if(currentDevice.isNormal()){
        
        }
        
        SitePreference sitePreference = SitePreferenceUtils.getCurrentSitePreference(request);
        if(sitePreference == null || sitePreference.isNormal()){
        
        }
        
        if(sitePreference.isMobile()){
        
        }
        
        if(sitePreference.isTablet()){
        
        }
        
        User user = userDAO.findById(1L);
        
        System.out.println("user id is "+user.getId());
        System.out.println("user firstname is "+user.getFirstname());
        System.out.println("user age is "+user.getAge());
        
        return "/user/addUser";
    }
    
    @RequestMapping(value="/addUser",method=RequestMethod.POST)
    public String addUserSubmit(HttpServletRequest request, @RequestParam(value="name",required = false) String username, 
            @RequestParam("file") MultipartFile[] formFileArray,
            @ModelAttribute("defaultUser") @Valid User user, BindingResult result){
        
        String firstName = request.getParameter("firstname");
        int age = Integer.parseInt(request.getParameter("age"));
        
        String un = request.getParameter("name");
        System.out.println("firstname is "+firstName);
        System.out.println("age is "+age);
        
        System.out.println("username is "+username);
        System.out.println("un is "+un);
        
        String mFN = user.getFirstname();
        int mAge = user.getAge();
        //String mCountry = user.getCountry();
        //boolean mMarried = user.isMarried();
        System.out.println("mFN is "+mFN);
        System.out.println("mAge is "+mAge);
        //System.out.println("mCountry is "+mCountry);
        //System.out.println("mMarried is "+mMarried);
        
        System.out.println("hasErrors? "+result.hasErrors());
        
        //save file
        try{
            String dirPath = System.getProperty("catalina.home");
            File filesFolder = new File(dirPath+File.separator + "files");
            if(!filesFolder.exists()){
                filesFolder.mkdirs();
            }
            for(MultipartFile formFile: formFileArray){
                String absPath= filesFolder.getAbsolutePath() + File.separator + formFile.getOriginalFilename();
                System.out.println("absPath is "+absPath);
                System.out.println("file content type is "+formFile.getContentType());
                System.out.println("file original name is "+formFile.getOriginalFilename());
                File file = new File(absPath);
                BufferedOutputStream fileStream = new BufferedOutputStream (new FileOutputStream(file));
                fileStream.write(formFile.getBytes());
                fileStream.close();
                System.out.println("uploaded file");
            }
            
        }catch(Exception e){
            System.out.println("An error occurred due to "+e.getMessage());
        }
        
        System.out.println("adding user");
        userDAO.add(user);
        
        if(result.hasErrors()){
            //has errors
            return "/user/addUser";
        } else{
            //no errors
            return "redirect:/user/";
        }
    }
    
    @RequestMapping("/mobile/userList")
    public String userListMobile(HttpServletRequest request){
        return "/mobile/list";
    }
    
    @ModelAttribute("defaultUser")
    public User defaultUser(){
        User user = new User();
        user.setFirstname("James");
        user.setAge(20);
        //user.setCountry("us");
        //user.setMarried(false);
        
        String[] defaultLanguages={"en","de"};
        //user.setLanguages(defaultLanguages);
        
        return user;
    }
    
    @ModelAttribute("countries")
    public Map<String,String> countries(){
        Map<String,String> m = new HashMap<String,String>();
        m.put("us", "United States");
        m.put("ca", "Canada");
        m.put("fr", "France");
        
        return m;
    }
    
    @ModelAttribute("languages")
    public Map<String,String> languages(){
        Map<String,String> m = new HashMap<String,String>();
        m.put("en", "English");
        m.put("fr", "French");
        m.put("de", "Germany");
        
        return m;
    }
}
