
package com.springcookbook.controller;

import com.springcookbook.model.User;
//import com.springcookbook.service.UserService;
//import com.springcookbook.service.UserServiceImpl1;
import com.springcookbook.service.UserServiceRestImpl;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.mobile.device.site.SitePreferenceUtils;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author joaocassamano
 */
@RestController
@RequestMapping("users*")
public class UserRestController {
    
    @Autowired
    UserServiceRestImpl userServiceRestImpl;
    
    @RequestMapping
    public List<User> userList(){
        List<User> userList  = userServiceRestImpl.findAll();
        return userList;
    }
    
    @RequestMapping("/{id}")
    public User findUser(@PathVariable("id") Long userId){
        User user = userServiceRestImpl.findAUser(userId);
        return user;
    }
    
    @RequestMapping("/template")
    public User template(){
        //String url = "https://localhost:8443/springwebapp/users/2";
        String url = "https://localhost:8080/springwebapp/users/2";
        RestTemplate rt = new RestTemplate();
        
        //single record
        User user = rt.getForObject(url, User.class);
        
        //list
        User[] userList = rt.getForObject(url, User[].class);
        return user;
    }
    
    
}
