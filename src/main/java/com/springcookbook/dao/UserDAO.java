
package com.springcookbook.dao;

//import com.springcookbook.model.Post;
import com.springcookbook.model.Post;
import com.springcookbook.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author joaocassamano
 */
@Repository
@Transactional
@EnableWebMvc
@ComponentScan
(basePackages={"com.springcookbook.controller",
               "com.springcookbook.service",
               "com.springcookbook.dao"
               /*",com.springcookbook.aspect"*/
})
public class UserDAO {
    
    @Autowired
    SessionFactory sessionFactory;
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    /*
    public void add(User user){
        String sql="insert into user (firstname,age) values (?,?)";;
        jdbcTemplate.update(sql,user.getFirstname(), user.getAge());
    }*/
    
    //@Transactional
    public void add(User user){
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }
    
    public void add(List<User> userList){
        String sql = "insert into user (firstname, age) values (?,?)";
        List<Object[]> userRows = new ArrayList<Object[]>();
        for(User user: userList){
            userRows.add(new Object[]{user.getFirstname(), user.getAge()});
        }
        
        jdbcTemplate.batchUpdate(sql,userRows);
    }
    
    public User findById(Long id){
        String sql = "select * from user where id=?";
        User user = jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserMapper());
        return user;
    }
    
    public List<User> findAll(Long id){
        String sql = "select * from user where id=?";
        List<User> userList = jdbcTemplate.query(sql, new UserMapper());
        return userList;
    }
    
    public List<User> findAll(){
        String sql = "select u.id, u.firstname, u.age, p.id as p_id, p.title as p_title, p.date as p_date from user u"
                + "left join post p on p.user_id = u.id order by u.id asc, p.date desc";
        return this.jdbcTemplate.query(sql,new UserWithPosts());
    }
    
    public void update(User user){
        String sql = "update user set firstname=?, age=? where id =?";
        jdbcTemplate.update(sql,user.getFirstname(),user.getAge(), user.getId());
    }
    
    public void save(User user){
        if(user.getId()==null){
            add(user);
        } else{
            update(user);
        }
    }
    
    public void delete(User user){
        String sql = "delete from user where id =?";
        jdbcTemplate.update(sql,user.getId());
    }
    
    public long countUsersUnder25(){
        String sql = "select count(*) from user where  age < 25";
        return jdbcTemplate.queryForObject(sql, Long.class);
    }

    private class UserWithPosts implements ResultSetExtractor<List<User>> {
        
        public List<User> extractData(ResultSet rs) throws SQLException, DataAccessException{
            Map<Long,User> userMap = new ConcurrentHashMap<Long,User>();
            User u = null;
            while (rs.next()){
                Long id = rs.getLong("id");
                u = userMap.get(id);
                
                if(u == null){
                    u = new User();
                    u.setId(id);
                    u.setFirstname(rs.getString("firstname"));
                    u.setAge(rs.getInt("age"));
                    userMap.put(id, u);
                }
                
                Long postId = rs.getLong("p_id");
                if(postId>0){
                    System.out.println("add post id="+postId);
                    Post p = new Post();
                    p.setId(postId);
                    p.setTitle(rs.getString("p_title"));
                    p.setDate(rs.getDate("p_date"));
                    p.setUser(u);
                    u.getPosts().add(p);
                }
            }
            return new LinkedList<User>(userMap.values());
        }
    }
    
}
class UserMapper implements RowMapper<User>{

    @Override
    public User mapRow(ResultSet row, int rowNum) throws SQLException {
        User user = new User();
        
        user.setId(row.getLong("id"));
        user.setFirstname(row.getString("firstname"));
        user.setAge(row.getInt("age"));
        
        return user;
    }
    
}
