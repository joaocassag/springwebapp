
package com.springcookbook.model;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author joaocassamano
 */
@Entity
@Table(name="user")
public class User {
    @Id
    @GeneratedValue
    private Long id;
    
    @NotNull
    @NotEmpty
    @Size(min = 3, max = 18)
    @Column(name="firstname")
    private String firstname;
    
    @Min(18) @Max(130)
    private Integer age;
    
    @Transient
    String country;
    
    @Transient
    boolean married;
    
    @Transient
    String[] languages;
    private LinkedList<Post> post = new LinkedList<>();
    
    public User(){
    
    }
    
    public User(Long id, String firstname, int age){
        this.id = id;
        this.firstname = firstname;
        this.age = age;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }
    
    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public LinkedList<Post> getPosts() {
        return post;
    }

}