
package com.springcookbook.service;

import org.springframework.stereotype.Component;

/**
 *
 * @author joaocassamano
 */

public interface UserService {
    public int findUsers();
    
}
