
package com.springcookbook.service;

import org.springframework.stereotype.Component;

/**
 *
 * @author joaocassamano
 */
@Component("aUserService1")
public class UserServiceImpl1 implements UserService {
    
    public int findUsers(){
        return 5;
    }
    
}
