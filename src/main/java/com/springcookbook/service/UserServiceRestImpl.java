
package com.springcookbook.service;

import com.springcookbook.model.User;
import java.util.LinkedList;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author joaocassamano
 */
@Component//("aUserServiceRest")
public class UserServiceRestImpl implements UserService {
    
    List<User> userList = new LinkedList<User>();
    
    public UserServiceRestImpl(){
        User user1 = new User(1L, "User1", 777);
        userList.add(user1);
        
        User user2 = new User(2L, "User2", 888);
        userList.add(user2);
    }
    
    public int findUsers(){
        return 5;
    }
    
    public List<User> findAll(){
        return userList;
    }
    
    public User findAUser(Long id){
        for(User user: userList){
            if(user.getId().equals(id)){
                return user;
            }
        }
        return null;
    }
    
}
