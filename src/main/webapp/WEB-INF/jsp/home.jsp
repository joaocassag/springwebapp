<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <body>
        <sec:authorize access="isAuthenticated()">
            Username: <sec:authentication property="principal.username" />
        </sec:authorize>
            
        <sec:authorize access="isAnonymous()">
            Only unauthenticated users see this
        </sec:authorize>
        
        <%--    --%>
        <sec:authorize access="hasAnyRole('ADMIN','SECURITY')">
            Only users who have ADMIN or SECURITY roles see this
        </sec:authorize>
        <%----%>
        <%--
        <spring:url value="/${lang}/home" var="home" />
        <a href="${home}">Home</a>
        --%>
        <h1><spring:message code="home.title" /></h1>
        <p><spring:message code="home.intro" /></p>

        <p>
            <a href="?lang=en">English</a>
            <a href="?lang=fr">French</a>
            <a href="?lang=pt">Portuguese</a>
        </p>
    </body>
    
</html>
