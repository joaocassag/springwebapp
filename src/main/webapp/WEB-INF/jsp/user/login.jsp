<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>

<c:url var="loginUrl" value="/user/login" />
<form:form action="${loginUrl}" mehtod="post">
<%--<form:form mehtod="POST" action="/user/">--%>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
           name is ${_csrf.parameterName}<br/>
           token is ${_csrf.token}<br/>
           <c:if test="${param.error != null}">
               <p>
               Invalid username and password.
               </p>
           </c:if>
               
            <p>
                <label for="username">Username</label>
                <input type="text" id="username" name="username" />
            </p>
            
            <p>
                <label for="password">Password</label>
                <input type="password" id="password" name="password" />
            </p>
            
            <button type="submit">Log in </button>
</form:form>

<html>
    <body>
        <%--
        <spring:url value="/${lang}/home" var="home" />
        <a href="${home}">Home</a>
        --%>
        <h1><spring:message code="home.title" /></h1>
        <p><spring:message code="home.intro" /></p>

        <p>
            <a href="?lang=en">English</a>
            <a href="?lang=fr">French</a>
            <a href="?lang=pt">Portuguese</a>
        </p>
    </body>
    
</html>
